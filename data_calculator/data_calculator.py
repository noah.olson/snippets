import struct
from typing import Dict
import tkinter as tk
from tkinter import ttk


# Globals
TEXT_BOXES: Dict[str, 'TextBox'] = {}
W_FRAME: ttk.Frame
SHOW_ALL_VAR: tk.StringVar
NEXT_ROW = 2

# Constants for TextBox constructor
NO_ENDIAN = 0
ENDIAN_1 = 1
ENDIAN_2 = 2


class TextBox:
    def __init__(self, name: str, validate_command, to_hex_fun, from_hex_fun,
                 endian_pattern: int):
        self.name = name
        self.endian = ''
        if name.find('_') != -1:
            if name.split('_')[-1][0] == 'l':
                self.endian = 'little'
            else:
                self.endian = 'big'
        self.to_hex_fun = to_hex_fun
        self.from_hex_fun = from_hex_fun

        # store the validate command (which is used by the validate wrapper),
        # and register the validate wrapper
        self.validate_command = validate_command
        validate_command = (W_FRAME.register(self.validate_wrapper), '%d', '%P')

        # make the label if this is the first text box in this row
        global NEXT_ROW
        if endian_pattern == NO_ENDIAN or endian_pattern == ENDIAN_1:
            label = ttk.Label(W_FRAME, text=name.split('_')[0] + '  ')
            label.grid(row=NEXT_ROW, column=0, sticky=(tk.W,))

        # put the entry box in the correct column and span it if there is only
        # one in this row
        self.variable = tk.StringVar()
        entry = ttk.Entry(W_FRAME, textvariable=self.variable,
                          validate='key', validatecommand=validate_command)
        if endian_pattern == NO_ENDIAN:
            entry.grid(row=NEXT_ROW, column=1, columnspan=2, sticky=(tk.W, tk.E),
                       padx=2, pady=2)
        else:
            entry.grid(row=NEXT_ROW, column=endian_pattern, sticky=(tk.W, tk.E),
                       padx=2, pady=2)

        # advance next row if this is the last text box in this row
        if endian_pattern == NO_ENDIAN or endian_pattern == ENDIAN_2:
            NEXT_ROW += 1
            pass

        # add to the dictionary
        global TEXT_BOXES
        TEXT_BOXES[name] = self

    def to_hex(self) -> str:
        return self.to_hex_fun(self.variable.get(), self.endian)

    def update(self, hexstring: str):
        self.variable.set(self.from_hex_fun(hexstring, self.endian))

    # This wrapper validates the input, then updates the other text boxes
    # if the input is valid. Why?:
    # Using a trace on variables like tk.StringVar would cause infinite recursion
    # when updating the other variables.
    # Yes I tried trace_remove() before modifying the value of the StringVar,
    # no it does not work.
    # Using a key event to trigger updates also does not work because key events
    # get called before the validate function, so we would not get the updated value
    # with a key event. This is just stupid.
    # Therefore, we are hijacking the validate function to also update the other
    # text boxes
    def validate_wrapper(self, action_code: str, txtbx_string: str) -> bool:
        valid = self.validate_command(action_code, txtbx_string)
        if valid:
            update_variables(self.name, self.to_hex_fun(txtbx_string, self.endian))
        return valid


# updates each text box other than the source text box
def update_variables(source_variable: str, hexstring: str):
    hexstring = '0'*(len(hexstring) % 2) + hexstring
    print('update')
    for text_box in TEXT_BOXES:
        if text_box != source_variable:
            TEXT_BOXES[text_box].update(hexstring)


# callback for when checkbox changes
# updates all variables
def update_on_checkbox_change(*args):
    # don't update if there is no input
    if TEXT_BOXES['hex'].to_hex():
        update_variables('', TEXT_BOXES['hex'].to_hex())


# action code 1 means insertion, which is the only case we need to check
# the intermediate form for conversion functions is a hex string
# the input for x_to_hexstring is a string because it comes from a text box

def validate_hex(action_code: str, hex_string: str) -> bool:
    if action_code == '1':
        for char in hex_string:
            if not (char.isnumeric() or char.lower() in 'abcdef'):
                return False
    return True

def hex_to_hexstring(hex_string: str, endian: str) -> str:
    return hex_string

def hex_from_hexstring(hexstring: str, endian: str) -> str:
    return hexstring.upper()


def validate_binary(action_code: str, binary_string: str) -> bool:
    if action_code == '1':
        for char in binary_string:
            if not char in '01':
                return False
    return True

def binary_to_hex(binary_string: str, endian: str) -> str:
    return hex(int(binary_string, 2))[2:]

def binary_from_hex(hexstring: str, endian: str) -> str:
    return bin(int(hexstring, base=16))[2:]


def validate_ascii(action_code: str, ascii_string: str) -> bool:
    if action_code == '1':
        return ascii_string.isascii()
    return True

def ascii_to_hex(ascii_string: str, endian: str) -> str:
    return bytes(ascii_string, 'ascii').hex()

def ascii_from_hex(hexstring: str, endian: str) -> str:
    byte_string = bytes.fromhex(hexstring)
    ascii_string = ''
    for byte in byte_string:
        try:
            char = bytes([byte]).decode('ascii')
        except:
            char = ' '
        ascii_string += char
    return ascii_string


def validate_uint(action_code: str, uint_string: str) -> bool:
    if action_code == '1':
        return uint_string.isnumeric()
    return True

def uint_to_hex(uint_string: str, endian: str) -> str:
    return hex(int(uint_string))[2:]

def uint_from_hex(hexstring: str, endian: str) -> str:
    return str(int.from_bytes(bytes.fromhex(hexstring), endian, signed=False))


def validate_int(action_code: str, int_string: str) -> bool:
    if action_code == '1':
        if not (int_string[0] == '-' or int_string[0].isnumeric()):
            return False
        if len(int_string) > 1:
            return int_string[1:].isnumeric()
    return True

def int_to_hex(int_string: str, endian: str) -> str:
    int_ = int(int_string)
    if int_ < 0:
        int_ = -int_ - 1
        bits_required = 0
        while int_ >> (bits_required):
            bits_required += 8
        return hex((int(int_string) + (1 << bits_required)) % (1 << bits_required))[2:]
    return hex(int_)[2:]

def int_from_hex(hexstring: str, endian: str) -> str:
    return str(int.from_bytes(bytes.fromhex(hexstring), endian, signed=True))


def validate_float(action_code: str, float_string: str) -> bool:
    if action_code == '1':
        for i in range(len(float_string)):
            if float_string[i].isnumeric():
                continue
            elif float_string[i] == '.':
                if float_string[i + 1:].find('.') != -1:
                    return False
            elif float_string[i] == '-':
                if i != 0:
                    return False
            else:
                return False
    return True

def float_to_hex(float_string: str, size: int, endian: str) -> str:
    if float_string[-1] == '.' or float_string[-1] == '-':
        float_string += '0'
    return struct.pack(('<' if endian == 'little' else '>')
                       + ('e' if size == 16 else ('f' if size == 32 else 'd'))
                       , float(float_string)).hex()

def float_from_hex(hexstring:str, size: int, endian: str) -> str:
    if len(hexstring) == size // 4 or (SHOW_ALL_VAR.get() == '1' and len(hexstring) > size // 4):
        var = bytes.fromhex(hexstring)
        return str(struct.unpack(('<' if endian == 'little' else '>')
                                 + ('e' if size == 16 else ('f' if size == 32 else 'd'))
                                 , bytes.fromhex(hexstring[:size // 4]))[0])
    else:
        return ''


def float16_to_hex(float_string: str, endian: str) -> str:
    return float_to_hex(float_string, 16, endian)

def float16_from_hex(hexstring: str, endian: str) -> str:
    return float_from_hex(hexstring, 16, endian)


def float32_to_hex(float_string: str, endian: str) -> str:
    return float_to_hex(float_string, 32, endian)

def float32_from_hex(hexstring: str, endian: str) -> str:
    return float_from_hex(hexstring, 32, endian)


def float64_to_hex(float_string: str, endian: str) -> str:
    return float_to_hex(float_string, 64, endian)

def float64_from_hex(hexstring: str, endian: str) -> str:
    return float_from_hex(hexstring, 64, endian)


def main():
    window = tk.Tk()
    window.title('Data Calculator')

    global W_FRAME
    W_FRAME = ttk.Frame(window, padding='12 12 12 12')
    W_FRAME.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))

    # window frame resizing
    window.columnconfigure(0, weight=1)
    window.rowconfigure(0, weight=1)

    # grid column resizing
    W_FRAME.columnconfigure(0, weight=0)
    W_FRAME.columnconfigure(1, weight=1)
    W_FRAME.columnconfigure(2, weight=1)

    # column labels
    ttk.Label(W_FRAME, text='type').grid(row=0, column=0, sticky=tk.W)
    ttk.Label(W_FRAME, text='little endian').grid(row=0, column=1)
    ttk.Label(W_FRAME, text='big endian').grid(row=0, column=2)
    # spacing label
    ttk.Label(W_FRAME).grid(row=1, column=0)


    TextBox('hex', validate_hex, hex_to_hexstring, hex_from_hexstring, NO_ENDIAN)

    TextBox('binary', validate_binary, binary_to_hex, binary_from_hex, NO_ENDIAN)

    TextBox('ascii', validate_ascii, ascii_to_hex, ascii_from_hex, NO_ENDIAN)

    TextBox('uint_le', validate_uint, uint_to_hex, uint_from_hex, ENDIAN_1)
    TextBox('uint_be', validate_uint, uint_to_hex, uint_from_hex, ENDIAN_2)

    TextBox('int_le', validate_int, int_to_hex, int_from_hex, ENDIAN_1)
    TextBox('int_be', validate_int, int_to_hex, int_from_hex, ENDIAN_2)

    TextBox('float16_le', validate_float, float16_to_hex, float16_from_hex, ENDIAN_1)
    TextBox('float16_be', validate_float, float16_to_hex, float16_from_hex, ENDIAN_2)

    TextBox('float32_le', validate_float, float32_to_hex, float32_from_hex, ENDIAN_1)
    TextBox('float32_be', validate_float, float32_to_hex, float32_from_hex, ENDIAN_2)

    TextBox('float64_le', validate_float, float64_to_hex, float64_from_hex, ENDIAN_1)
    TextBox('float64_be', validate_float, float64_to_hex, float64_from_hex, ENDIAN_2)


    global SHOW_ALL_VAR
    SHOW_ALL_VAR = tk.StringVar()
    SHOW_ALL_VAR.trace_add('write', update_on_checkbox_change)
    check_box = ttk.Checkbutton(W_FRAME, text='show all', variable=SHOW_ALL_VAR)
    check_box.grid(row=NEXT_ROW, column=0)


    window.mainloop()


if __name__ == '__main__':
    main()
