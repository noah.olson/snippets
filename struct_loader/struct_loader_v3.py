# struct_file.py
# Copyright 2020 Noah Olson

from typing import List, Dict, Tuple, Any
import io


DATA_MEMBER_TYPES = ['builtin', 'array', 'struct', 'saveposition']


#############################
########## Helpers ##########
#############################
def get_datatype_base_and_length(data_type: str) -> Tuple[str, str]:
    base = ''
    while data_type and not data_type[0].isnumeric():
        base += data_type[0]
        data_type = data_type[1:]
    return base, data_type


def get_keywords(line: str) -> List[str]:
    line = line.split('//')[0].lstrip()
    terms = []
    while line:
        term = ''
        while line and not line[0].isspace():
            term += line[0]
            line = line[1:]
        terms.append(term)
        line = line.lstrip()
    return terms


def is_valid_struct_name(name: str) -> bool:
    pass


def is_valid_struct_header(keywords: List[str]) -> bool:
    if len(keywords) != 4:
        return False
    if keywords[0] != 'struct':
        return False
    if not is_valid_struct_name(keywords[1]):
        return False
    keywords[2] = keywords[2].lower()
    if not (keywords[2] == 'endian=little' or keywords[2] == 'endian=big'):
        return False
    if keywords[3] != '{':
        return False
    return True


def is_valid_member_name(name: str) -> bool:
    pass


####################################
########## Helper classes ##########
####################################
class StructMember:
    # In cases where it is possible to determine that the keywords (line)
    # are intended to be this member, but are invalid in some way, we will
    # raise an exception here to give more specific information to the user.
    # If we didn't, an exception would still be generated later and be vague,
    # so this is the better option.
    @classmethod
    def is_member(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        pass

    def get_type(self) -> str:
        pass

    def get_subtype(self) -> str:
        pass

    def get_member_name(self) -> str:
        pass


########## Struct ##########
class StructType(StructMember):
    def __init__(self, keywords: List[str], context: 'TemplateLoaderContext'):
        self.name = keywords[1]
        context.templates[keywords[0]].load(context)
        self.members = context.templates[keywords[0]].members

    def get_member_name(self) -> str:
        return self.name

    @classmethod
    def is_member(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        if len(keywords) != 2:
            return False
        if keywords[1] not in context.templates:
            return False
        if not is_valid_member_name(keywords[1]):
            return False
        return True

    def get_type(self) -> str:
        return 'struct'

    def get_subtype(self) -> str:
        return ''


########## Builtins ##########
class BuiltinType(StructMember):
    def __init__(self, keywords: List[str], context: 'TemplateLoaderContext'):
        self.name = keywords[1]
        self.length = get_datatype_base_and_length(keywords[0])[1]

    def get_member_name(self) -> str:
        return self.name

    type_name: str
    valid_lengths: List[str]

    @classmethod
    def is_member(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        if len(keywords) != 2:
            return False
        base, length = get_datatype_base_and_length(keywords[0])
        if base != cls.type_name:
            return False
        if length not in cls.valid_lengths:
            return False
        if context.is_member_of_type_and_name(DATA_MEMBER_TYPES, keywords[1]):
            return False
        if not is_valid_member_name(keywords[1]):
            return False
        return True

    def get_type(self) -> str:
        return 'builtin'

    def get_subtype(self) -> str:
        return self.type_name


class IntBuiltinType(BuiltinType):
    type_name = 'int'
    valid_lengths = ['16', '32', '64']


class UintBuiltinType(BuiltinType):
    type_name = 'uint'
    valid_lengths = ['16', '32', '64']


class FloatBuiltinType(BuiltinType):
    type_name = 'float'
    valid_lengths = ['16', '32', '64']


class CharBuiltinType(BuiltinType):
    type_name = 'char'
    valid_lengths = ['']


class RawBuiltinType(BuiltinType):
    type_name = 'raw'

    @classmethod
    def is_member(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        if len(keywords) != 2:
            return False
        base, length = get_datatype_base_and_length(keywords[0])
        if base != cls.type_name:
            return False
        if not length.isnumeric():
            return False
        if length % 8:
            return False
        if context.is_member_of_type_and_name(DATA_MEMBER_TYPES, keywords[1]):
            return False
        if not is_valid_member_name(keywords[1]):
            return False
        return True


########## Statements ##########
class Statement(StructMember):
    statement: str
    keywords: int

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        pass

    @classmethod
    def is_member(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        if len(keywords) != cls.keywords:
            return False
        if keywords[0] != cls.statement:
            return False
        return cls.additional_check(keywords, context)

    def get_member_name(self) -> str:
        return ''

    def get_type(self) -> str:
        return self.statement

    def get_subtype(self) -> str:
        return ''


class SavepositionStatement(Statement):
    def __init__(self, keywords: List[str], context: 'TemplateLoaderContext'):
        self.name = keywords[1]

    def get_member_name(self) -> str:
        return self.name

    statement = 'saveposition'
    keywords = 2

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        return not context.is_member_of_type_and_name(DATA_MEMBER_TYPES, keywords[1])


class GobackStatement(Statement):
    def __init__(self, keywords: List[str], context: 'TemplateLoaderContext'):
        # there is nothing unique between goback statements
        pass

    statement = 'goback'
    keywords = 1

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        gotos = 0
        gobacks = 0
        for member in context.current_template.members:
            if member.get_type() in ['seek', 'goto']:
                gotos += 1
            elif member.get_type() == 'goback':
                gobacks += 1
            if gobacks > gotos:
                return False
        if not gotos > gobacks:
            return False
        return True


class SeekFixedStatement(Statement):
    statement = 'seek'
    keywords = 2

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        return (keywords[1][0].isnumeric() or keywords[1][0] == '-') and keywords[1][1:].isnumeric()


class SeekMemberStatement(Statement):
    statement = 'seek'
    keywords = 2

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        return context.is_member_of_subtype_and_name(['int', 'uint'], keywords[1])


class GotoSavedpositionStatement(Statement):
    statement = 'goto'
    keywords = 2

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        return context.is_member_of_type_and_name(['saveposition'], keywords[1])


class GotoEndStatement(Statement):
    statement = 'goto'
    keywords = 2

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        return keywords[1] == 'end'


class GotoStartStatement(Statement):
    statement = 'goto'
    keywords = 2

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        return keywords[1] == 'start'


class GotoFixedStatement(Statement):
    statement = 'goto'
    keywords = 2

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        return keywords[1].isnumeric()


class GotoMemberStatement(Statement):
    statement = 'goto'
    keywords = 2

    @classmethod
    def additional_check(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        return context.is_member_of_subtype_and_name(['uint'], keywords[1])


########## Arrays ##########
class ArrayDescriptor:
    @classmethod
    def descriptor_validator_function(cls, descriptor: str, context: 'TemplateLoaderContext'):
        pass


class FixedCalculatedArrayDescriptor(ArrayDescriptor):
    @classmethod
    def descriptor_validator_function(cls, descriptor: str, context: 'TemplateLoaderContext'):
        return descriptor.isnumeric()


class MemberCalculatedArrayDescriptor(ArrayDescriptor):
    @classmethod
    def descriptor_validator_function(cls, descriptor: str, context: 'TemplateLoaderContext'):
        if not context.is_member_of_subtype_and_name(['uint'], descriptor):
            return False
        return True


class HexcodeTerminatedArrayDescriptor(ArrayDescriptor):
    @classmethod
    def descriptor_validator_function(cls, descriptor: str, context: 'TemplateLoaderContext'):
        descriptor = descriptor.lower()
        if len(descriptor) < 4 or len(descriptor) % 2:
            return False
        if descriptor[:2] != '0x':
            return False
        for char in descriptor:
            if char not in '0123456789abcdef':
                return False
        return True


class MemberTerminatedArrayDescriptor(ArrayDescriptor):
    @classmethod
    def descriptor_validator_function(cls, descriptor: str, context: 'TemplateLoaderContext'):
        if not context.is_member_of_subtype_and_name(['raw'], descriptor):
            return False
        return True


class ArrayType(StructMember):
    DESCRIPTOR_CLASSES: List[ArrayDescriptor] = [
        FixedCalculatedArrayDescriptor,
        MemberCalculatedArrayDescriptor,
        HexcodeTerminatedArrayDescriptor,
        MemberTerminatedArrayDescriptor,
    ]

    def __init__(self, keywords: List[str], context: 'TemplateLoaderContext'):
        self.name = keywords[1]
        self.sub_type = keywords[0][:keywords[0].find('[')]
        # Todo: get descriptors

    def get_type(self) -> str:
        return 'array'

    def get_subtype(self) -> str:
        return self.sub_type

    def get_member_name(self) -> str:
        return self.name

    @classmethod
    def validate_array_descriptors(cls, descriptor_string: str, context: 'TemplateLoaderContext') -> bool:
        # get rid of the first [ because split() would create an extra empty string for it
        descriptor_string = descriptor_string[:-1]
        descriptors = descriptor_string.split('[')
        for descriptor in descriptors:
            if not descriptor:
                return False
            if descriptor[-1] != ']':
                return False
            recognized_descriptor = False
            for d_class in cls.DESCRIPTOR_CLASSES:
                if d_class.descriptor_validator_function(descriptor[:-1], context):
                    recognized_descriptor = True
                    break
            if not recognized_descriptor:
                return False
        return True

    @classmethod
    def is_member(cls, keywords: List[str], context: 'TemplateLoaderContext') -> bool:
        if len(keywords) != 2:
            return False
        if '[' not in keywords[0]:
            return False
        element_type = keywords[0][:keywords[0].find('[')]
        array_descriptors = keywords[0][keywords[0].find('['):]
        if not cls.validate_array_descriptors(array_descriptors, context):
            return False
        if context.is_member_of_type_and_name(DATA_MEMBER_TYPES, keywords[1]):
            return False
        # Todo: check element type


########## Contexts ##########
class TemplateLoaderContext:
    def __init__(self, templates: Dict[str, 'StructTemplate'], current_template: str, file: io.TextIOBase):
        self.templates = templates
        self.current_template = templates[current_template]
        self.file = file

    def is_member_of_type_and_name(self, types: List[str], name: str) -> bool:
        for member in self.current_template.members:
            if member.get_type() in types:
                if member.get_member_name() == name:
                    return True
        return False

    def is_member_of_subtype_and_name(self, subtypes: List[str], name: str):
        for member in self.current_template.members:
            if member.get_subtype() in subtypes:
                if member.get_member_name() == name:
                    return True
        return False


class DataLoaderContext:
    def __init__(self):
        pass


class StructTemplate:
    def __init__(self, struct_name: str, endian: str, location: int):
        self.struct_name = struct_name
        self.endian = endian
        self.location = location

        self.members: List[StructMember] = []

        self.is_loaded = False
        self.is_being_loaded = False

    def load(self, context: TemplateLoaderContext):
        if self.is_loaded:
            return
        if self.is_being_loaded:
            raise ValueError('recursive struct')
        self.is_being_loaded = True

        context = TemplateLoaderContext(context.templates, self.struct_name, context.file)

        old_location = context.file.tell()
        context.file.seek(self.location)
        # skip the header
        context.file.readline()

        line = context.file.readline()
        while line:
            keywords = get_keywords(line)
            if len(keywords) == 0:
                continue
            if keywords[0] == '}':
                break

            # Todo: try to create each type of member

            line = context.file.readline()

        context.file.seek(old_location)
        self.is_being_loaded = False
        self.is_loaded = True


#############################
########## Helpers ##########
#############################
def create_templates(s_file) -> Dict[str, StructTemplate]:
    file = open(s_file, 'r')
    templates = {}

    last_location = file.tell()
    line = file.readline()
    while line:
        keywords = get_keywords(line)
        if is_valid_struct_header(keywords):
            if keywords[1] in templates:
                raise ValueError('multiple structs with same name')
            new_template = StructTemplate(keywords[1], keywords[2], last_location)
            templates[keywords[1]] = new_template

        last_location = file.tell()
        line = file.readline()

    file.close()
    return templates


######################################
########## Public functions ##########
######################################
def load_struct(s_file: str, root_struct_name: str = ''):
    templates = create_templates(s_file)
    if root_struct_name != '':
        if root_struct_name not in templates:
            raise ValueError('root struct does not exist')


def load_file():
    pass
