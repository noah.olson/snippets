# struct_loader version 0.2
# Copyright (c) 2020 Noah Olson. See LICENSE file in root source directory.

from typing import Tuple, List, Dict
import io
import struct


def get_keywords(line: str) -> List[str]:
    """Returns a list of the strings that were separated by
    whitespace and ignores anything after '//'"""
    line = line.split('//')[0].lstrip()
    keywords = []
    while line:
        keyword = ''
        while line and not line[0].isspace():
            keyword += line[0]
            line = line[1:]
        keywords.append(keyword)
        line = line.lstrip()
    return keywords


# todo
def is_builtin_data_type(data_type: str) -> bool:
    base, length = get_base_and_length(data_type)
    if base == 'int' or base == 'uint':
        if length.isnumeric() and not int(length) % 8:
            return True
    elif base == 'float':
        if length.isnumeric() and not int(length) % 8:
            return True
    elif base == 'char':
        if length.isnumeric() and int(length) == 8:
            return True
    elif base == 'byte':
        if not length:
            return True
    elif base == 'hex':
        if length.isnumeric() and not int(length) % 8:
            return True
    return False


# todo
def is_valid_field_or_struct_name(name: str) -> bool:
    return True


# todo cannot have same name as builtin type
def is_valid_struct_header(keywords: List[str]) -> bool:
    if (len(keywords) == 4
            and keywords[0].lower() == 'struct'
            and is_valid_field_or_struct_name(keywords[1])
            and (keywords[2].lower() == 'endian=little'
                 or keywords[2].lower() == 'endian=big')
            and keywords[3] == '{'):
        return True
    return False


def get_base_and_length(data_type: str) -> Tuple[str, str]:
    base = ''
    while data_type and not data_type[0].isnumeric():
        base += data_type[0]
        data_type = data_type[1:]
    return base, data_type


def is_valid_hexadecimal(hex_string: str) -> bool:
    hex_string = hex_string.lower()
    if len(hex_string) < 4 or len(hex_string) % 2 or hex_string[:2] != '0x':
        return False
    for char in hex_string[2:]:
        if char not in '0123456789abcdef':
            return False
    return True


def get_array_descriptors(descriptors: str, fields: List[Tuple]) -> List[Tuple]:
    """Returns a list of array descriptor tuples parsed from the passed string.
    Returns False if the string is invalid."""
    descriptor_list = []
    for descriptor in descriptors.split('['):
        if not (len(descriptor) > 1 and descriptor[-1] == ']'):
            return False
        descriptor = descriptor[:-1]
        if descriptor.isnumeric():
            descriptor_list.append(('fixed', 'literal', descriptor))
        elif is_valid_hexadecimal(descriptor):
            descriptor_list.append(('variable', 'literal', descriptor[2:].lower()))
        else:
            next_descriptor_tuple = None
            for field in fields:
                if field[0] == 'data' and field[1] == descriptor:
                    if field[2] == 'int' or field[2] == 'uint':
                        next_descriptor_tuple = ('fixed', 'data', descriptor)
                        break
                    elif field[2] == 'byte' or field[2] == 'hex':
                        next_descriptor_tuple = ('variable', 'data', descriptor)
                        break
                    return False
                elif field[0] == 'array' and field[1] == descriptor:
                    if field[2][0] == 'data' and (field[2][2] == 'byte' or field[2][2] == 'hex'):
                        next_descriptor_tuple = ('variable', 'array', descriptor)
                        break
                    return False
            if next_descriptor_tuple:
                descriptor_list.append(next_descriptor_tuple)
            else:
                return False
    return descriptor_list


def is_already_field_with_name(fields: List[Tuple], name: str) -> bool:
    for field in fields:
        if field[0] in ['data', 'struct', 'array', 'saveposition'] and field[1] == name:
            return True
    return False


# The following functions parse lines in a struct file.
# For documentation on the layout of the tuples that these functions return,
# read the INTERNALS section of struct_loader_doc.txt.
def get_goback_statement(keywords: List[str], fields: List[Tuple]) -> Tuple:
    """Returns a goback statement tuple if keywords represent a valid goback
    statement. Returns False otherwise."""
    if not keywords[0] == 'goback':
        return False
    gotos = 0
    gobacks = 0
    for field in fields:
        if field[0] == 'goto':
            gotos += 1
        elif field[0] == 'goback':
            gobacks += 1
    if gotos > gobacks:
        return 'goback',
    return False


def get_goto_statement(keywords: List[str], fields: List[Tuple]) -> Tuple:
    """Returns a goto statement tuple if keywords represent a valid goto
    statement. Returns False otherwise."""
    if not keywords[0].lower() == 'goto':
        return False
    if len(keywords) == 2:
        for field in fields:
            if field[0] == 'saveposition' and field[1] == keywords[1]:
                return 'goto', 'savedposition', keywords[1]
        return False
    elif len(keywords) == 3:
        keywords[2] = keywords[2].lower()
        if not (keywords[2] == 'relative' or keywords[2] == 'absolute'):
            return False
        if keywords[1].isnumeric():
            if int(keywords[1]) < 0 and keywords[2] == 'absolute':
                return False
            return 'goto', 'literal', int(keywords[1]), keywords[2]
        for field in fields:
            if (field[0] == 'data'
                    and field[1] == keywords[1]
                    and (field[2] == 'int'
                         or field[2] == 'uint')):
                return 'goto', 'data', keywords[1], keywords[2]
    return False


def get_save_position_statement(keywords: List[str], fields: List[Tuple]):
    """Returns a saveposition statement tuple if keywords represent a valid
    saveposition statement. Returns False otherwise."""
    if (len(keywords) == 2
            and keywords[0].lower() == 'saveposition'
            and is_valid_field_or_struct_name(keywords[1])
            and not is_already_field_with_name(fields, keywords[1])):
        return 'saveposition', keywords[1]
    return False


def get_data_field(keywords: List[str], fields: List[Tuple]):
    """Returns a data field tuple if keywords represent a valid data field.
    Returns False otherwise."""
    if (len(keywords) == 2
            and is_builtin_data_type(keywords[0])
            and is_valid_field_or_struct_name(keywords[1])
            and not is_already_field_with_name(fields, keywords[1])):
        base, length = get_base_and_length(keywords[0])
        return 'data', keywords[1], base, length
    return False


def get_struct_field(keywords: List[str],
                     fields: List[Tuple],
                     known_structs: Dict[str, '_StructTemplate']):
    """Returns a struct field tuple if keywords represent a valid struct
    field. Returns False otherwise."""
    if (len(keywords) == 2
            and not is_builtin_data_type(keywords[0])
            and is_valid_field_or_struct_name(keywords[1])
            and not is_already_field_with_name(fields, keywords[1])
            and keywords[0] in known_structs):
        known_structs[keywords[0]].load()
        return ('struct',
                keywords[1],
                keywords[0],
                known_structs[keywords[0]].endian,
                known_structs[keywords[0]].fields)
    return False


def get_array_field(keywords: List[str],
                    fields: List[Tuple],
                    known_structs: Dict[str, '_StructTemplate']):
    """Returns an array field tuple if keywords represent a valid array field.
    Returns False otherwise."""
    if not (len(keywords) == 2
            and is_valid_field_or_struct_name(keywords[1])
            and not is_already_field_with_name(fields, keywords[1])
            and '[' in keywords[0]):
        return False
    data_type = keywords[0][:keywords[0].index('[')]
    element_object = (get_data_field([data_type, keywords[1]], fields)
                      or get_struct_field([data_type, keywords[1]], fields,
                                          known_structs))
    if not element_object:
        return False
    array_descriptors = keywords[0][keywords[0].index('[') + 1:]
    array_descriptors = get_array_descriptors(array_descriptors, fields)
    if not array_descriptors:
        return False
    return 'array', keywords[1], element_object, array_descriptors


class _StructTemplate:
    """An internal class used for the parsing of struct files."""
    def __init__(self,
                 data_type: str,
                 endian: str,
                 file: io.TextIOBase,
                 location: int,
                 known_structs: Dict[str, '_StructTemplate']):
        """This constructor is internal and should
        only be used by the from_file classmethod."""
        self.data_type = data_type
        # endianess of all fields in this struct
        self.endian = endian
        # file to load from
        self.file = file
        # location of the start of the struct in the struct file
        self.location = location
        # all structs present in the file
        self.known_structs = known_structs
        # whether the template has been loaded from file yet
        self.is_loaded = False
        # keeps track of whether this template is in the process of being
        # loaded, protects against recursive structs
        self.is_being_loaded = False

    def load(self):
        """This method should only be used by the from_file classmethod.
        It is what actually parses the structs into a template structure
        that StructObject can use to parse data."""
        if self.is_loaded:
            return
        if self.is_being_loaded:
            raise ValueError('struct', self.data_type, 'is recursive')
        self.is_being_loaded = True

        fields = []
        previous_position = self.file.tell()
        self.file.seek(self.location)
        while True:
            line = self.file.readline()
            if not line:
                raise ValueError('reached end of file while reading struct')
            keywords = get_keywords(line)
            if len(keywords) == 0:
                continue
            elif keywords[0] == '}':
                break

            next_field = (get_data_field(keywords, fields)
                          or get_array_field(keywords, fields, self.known_structs)
                          or get_struct_field(keywords, fields, self.known_structs)
                          or get_save_position_statement(keywords, fields)
                          or get_goback_statement(keywords, fields)
                          or get_goto_statement(keywords, fields))
            if not next_field:
                raise ValueError(keywords, 'is not a valid part of a struct')
            fields.append(next_field)
        self.fields = fields
        self.file.seek(previous_position)
        self.is_loaded = True
        self.is_being_loaded = False

    @classmethod
    def from_file(cls, s_file: str, root_struct_name: str):
        """Searches the input file for all struct declarations,
        then loads them and returns the root struct."""
        struct_file = open(s_file, 'r')
        known_structs = {}
        root_struct: _StructTemplate = None
        line = struct_file.readline()
        while line:
            keywords = get_keywords(line)
            if is_valid_struct_header(keywords):
                name = keywords[1]
                if name in known_structs:
                    raise ValueError('multiple structs with name', name, 'found')
                endian = keywords[2][7:].lower()
                template = _StructTemplate(name,
                                           endian,
                                           struct_file,
                                           struct_file.tell(),
                                           known_structs)
                known_structs[name] = template
                if not root_struct:
                    if name == root_struct_name or not root_struct_name:
                        root_struct = template
            line = struct_file.readline()
        if not root_struct:
            raise ValueError('no struct with name', root_struct_name)
        root_struct.load()
        return ('struct',
                'root',
                root_struct.data_type,
                root_struct.endian,
                root_struct.fields)


class StructObject:
    """This is the user facing class which holds the template structure
    and the data structure after it is loaded."""
    def __init__(self, struct_file: str, root_struct_name: str = '', copy=False):
        """This constructor requires a file string and optionally a root struct
        name. If a name is not passed, the first struct in the file will be returned.
        Do not pass copy; you will get an incomplete StructObject."""
        if not copy:
            template = _StructTemplate.from_file(struct_file, root_struct_name)
            self._template_structure = template
            self.name = template[2]

    def load_data_from_file(self, data_file: str, offset: int = 0):
        """Loads and parses raw data from a data file. Pass an offset
        value if your struct does not begin at the start of the file."""
        file = open(data_file, 'rb')
        file.seek(offset)
        self.fields = StructObject._load_struct_field(self._template_structure, file)

    def print(self):
        """Prints the structure and the data it contains.
        load_data_from_file must have been run before this."""
        StructObject._print_struct_field(self.fields, self.name)

    def get_copy(self) -> 'StructObject':
        """Returns a StructObject with the same template as this one. This is
        free and allows you to read many files without reloading the struct."""
        copy = StructObject('', copy=True)
        copy.name = self.name
        copy._template_structure = self._template_structure
        return copy

    @classmethod
    def _print_struct_field(cls, struct_: Dict, name: str, indent: int = 0):
        """Recursive method which prints the passed struct and all
        of its fields."""
        print('  ' * indent + name + ':')
        indent += 1
        for field in struct_:
            if isinstance(struct_[field], Dict):
                StructObject._print_struct_field(struct_[field], field, indent)
            elif isinstance(struct_[field], List):
                print('  ' * indent + field + ': ', end='')
                if len(struct_[field]) == 0:
                    print('[ ]')
                else:
                    if isinstance(struct_[field][0], Dict):
                        print()
                        for i in range(len(struct_[field])):
                            StructObject._print_struct_field(struct_[field][i],
                                                             field + '[' + str(i) + ']',
                                                             indent + 1)
                    elif isinstance(struct_[field][0], List):
                        print()
                        for i in range(len(struct_[field])):
                            print('  ' * (indent + 1) + str(i) + ': ' + struct_[field][i])
                    else:
                        print(struct_[field])
            else:
                print('  ' * indent + field + ':', struct_[field])

    @classmethod
    def _load_struct_field(cls, struct_: Tuple, file: io.BufferedReader) -> Dict:
        """Recursive method which loads all of the fields in the passed
        struct and returns a dictionary of the fields."""
        fields = struct_[4]
        return_fields = {}
        saved_positions = {}
        goto_history = []
        for field in fields:
            if field[0] == 'goto':
                if field[1] == 'savedposition':
                    file.seek(saved_positions[field[2]])
                else:
                    seek_mode = int(field[3] == 'relative')
                    if field[1] == 'literal':
                        file.seek(int(field[2]), seek_mode)
                    else:
                        file.seek(fields[field[2]], seek_mode)
                goto_history.append(file.tell())
            elif field[0] == 'goback':
                file.seek(goto_history.pop())
            elif field[0] == 'saveposition':
                saved_positions[field[1]] = file.tell()
            elif field[0] == 'data':
                return_fields[field[1]] = StructObject._load_data_field(field,
                                                                        file,
                                                                        struct_[3])
            elif field[0] == 'struct':
                return_fields[field[1]] = StructObject._load_struct_field(field,
                                                                          file)
            elif field[0] == 'array':
                return_fields[field[1]] = StructObject._load_array_field(field,
                                                                         file,
                                                                         struct_[3],
                                                                         return_fields)
        return return_fields

    @classmethod
    def _load_data_field(cls, field: Tuple, file: io.BufferedReader, endian: str):
        """Loads an individual data field according to its type.
        Return type for each data type is explained in
        struct_loader_doc.txt in the BUILTIN TYPES section."""
        if field[2] == 'byte':
            return file.read(1)
        else:
            data = file.read(int(field[3]) // 8)

            if field[2] == 'int':
                return int.from_bytes(data, endian, signed=True)
            elif field[2] == 'uint':
                return int.from_bytes(data, endian, signed=False)
            elif field[2] == 'float':
                if endian == 'little':
                    endian = '<'
                else:
                    endian = '>'
                if field[3] == '32':
                    format_char = 'f'
                else:
                    format_char = 'd'
                return struct.unpack(endian + format_char, data)[0]
            elif field[2] == 'char':
                return data.decode('ascii')
            elif field[2] == 'hex':
                return data.hex()

    @classmethod
    def _load_array_field(cls, field: Tuple, file: io.BufferedReader, endian: str,
                          fields: Dict, descriptor_index: int = 0) -> List:
        """Recursive method which loads an array of a certain type
        according to the descriptor pointed at by the descriptor index.
        Returns the array as a list."""
        elements = []
        descriptor = field[3][descriptor_index]
        element_object = None
        if descriptor_index == len(field[3]) - 1:
            element_object = field[2]
        if descriptor[0] == 'fixed':
            if descriptor[1] == 'literal':
                length = int(descriptor[2])
            else:
                length = int(fields[descriptor[2]])
            for i in range(length):
                if element_object:
                    if element_object[0] == 'data':
                        elements.append(StructObject._load_data_field(element_object,
                                                                      file,
                                                                      endian))
                    else:
                        elements.append(StructObject._load_struct_field(element_object, file))
                else:
                    elements.append(
                        StructObject._load_array_field(field, file, endian,
                                                       fields, descriptor_index + 1))
        else:
            if descriptor[1] == 'literal':
                terminator = bytes.fromhex(descriptor[2])
            elif descriptor[1] == 'data':
                data_field = fields[descriptor[2]]
                if isinstance(data_field, bytes):
                    terminator = data_field
                else:
                    terminator = bytes.fromhex(data_field)
            else:
                array_field = fields[descriptor[2]]
                if isinstance(array_field[0], bytes):
                    byte_array = bytearray()
                    for byte in array_field:
                        byte_array.append(byte)
                    terminator = bytes(byte_array)
                else:
                    terminator = bytes.fromhex(''.join(array_field))
            while file.peek(len(terminator))[:len(terminator)] != terminator:
                if element_object:
                    if element_object[0] == 'data':
                        elements.append(StructObject._load_data_field(element_object,
                                                                      file,
                                                                      endian))
                    else:
                        elements.append(StructObject._load_struct_field(element_object, file))
                else:
                    elements.append(
                        StructObject._load_array_field(field, file, endian,
                                                       fields, descriptor_index + 1))
            file.read(len(terminator))
        return elements
