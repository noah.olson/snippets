# Copyright (c) 2020 Noah Olson. See LICENSE file in root source directory.

import os.path
import io
from typing import Tuple, List, Union, Dict, BinaryIO
from abc import ABC, abstractmethod
# element
# member


class StructMember(ABC):

    @classmethod
    @abstractmethod
    def from_DataType(cls, name: str, data_type: '_DataType') -> 'StructMember':
        pass

    @abstractmethod
    # makes it possible to create copies for arrays or reading new files
    # without needing to create another _DataType object
    def get_copy(self, new_name: str = '') -> 'StructMember':
        pass

    @abstractmethod
    def _load_from_file(self, file: io.BufferedReader):
        pass

    @abstractmethod
    def print(self, level: int = 0):
        pass


class _DataType:
    """The purpose of this class is to check and parse user input data types.
    It aids in the initial creation of StructMember[s] and is not kept after.

    The only attributes guaranteed to exist after initialization are data_class
    and full_data_type.
    This will be one of the three StructMember classes. This allows creating
    a new instance of the data type without any additional type testing.
    The following additional attributes will be present by class:

    StructArray:
      * array_type: 'fixed' if constant length or 'variable' if terminated by value
      * array_end: number of elements if 'fixed' or terminator value if 'variable'
      * element_data_class: data_class of the elements of the array
      * full_array_descriptor: eg. '[10]' or '[0x00]' - used for printing
      * StructArray will additionally contain all attributes from either
        StructDataMember or StructObject depending on the type of the array.

    StructDataMember:
      * base_type: builtin base type eg. 'int' or 'char'
      * data_length_bytes: integer length of type in bytes

    StructObject:
      * struct_template: _StructTemplate object for initializing a StructObject
    """

    def __init__(self, data_type: str, known_structs: List['_StructTemplate']):
        self.data_class: StructMember
        self.full_data_type: str

        # separate data type and array descriptor
        array_descriptor = ''
        if data_type.find('[') != -1:
            array_descriptor = data_type[data_type.find('['):]
            data_type = data_type[:data_type.find('[')]

        # make sure the data type is not just an array descriptor
        if not data_type:
            raise ValueError('Data type contains an array descriptor but no data type')
        self.full_data_type = data_type

        # now see whether it is a builtin or user defined struct
        split = _DataType.separate_type_base_and_length(data_type)
        if _DataType.is_builtin_base_type(split):
            if _DataType.is_valid_builtin_type(split):
                self.base_type = split[0]
                self.data_length_bytes = int(split[1]) // 8
                self.data_class = StructDataMember
            else:
                raise ValueError('Trying to use builtin type of invalid size')
        else:
            template = None
            for struct in known_structs:
                if struct.data_type == data_type:
                    template = struct
                    break
            if template:
                self.struct_template: _StructTemplate = template
                self.data_class = StructObject
            else:
                raise ValueError('Data type is not a recognized builtin or struct')

        # parse the array descriptor if if it exists
        if array_descriptor:
            self.full_array_descriptor = array_descriptor

            # make sure that the descriptor is not just a bracket,
            # and that the descriptor ends with a bracket
            if array_descriptor[-1] != ']' or array_descriptor == '[]':
                raise ValueError('Array descriptor is empty or missing a closing bracket')
            array_descriptor = array_descriptor[1:-1]

            # the array descriptor must either be integer, in which case it is
            # used as the array length, or hexadecimal in which case it is
            # used as the array terminator
            if array_descriptor.isnumeric():
                self.array_type = 'fixed'
                self.array_end = int(array_descriptor)
            else:
                # make sure that there is at least one hexadecimal byte, that
                # the descriptor is an even length (hexadecimal bytes are 2
                # characters long), and that the descriptor begins with '0x'
                # (the hexadecimal prefix)
                if (len(array_descriptor) < 4 or len(array_descriptor) % 2
                        or array_descriptor[0:2] != '0x'):
                    raise ValueError('Array descriptor is not valid hexadecimal')
                # make sure that the descriptor is entirely hexadecimal values
                array_descriptor = array_descriptor[2:].lower()
                for char in array_descriptor:
                    if '0123456789abcdef'.find(char) == -1:
                        raise ValueError('Array descriptor contains non-hexadecimal characters')
                self.array_type = 'variable'
                self.array_end = array_descriptor

            self.element_data_class = self.data_class
            self.data_class = StructArray

    @classmethod
    def is_builtin_base_type(cls, data_type: Tuple[str, str]) -> bool:
        base_types = ['int', 'uint', 'float', 'char', 'hex']
        for base_type in base_types:
            if data_type[0] == base_type:
                return True
        return False

    @classmethod
    def is_valid_builtin_type(cls, data_type: Tuple[str, str]) -> bool:
        if _DataType.is_builtin_base_type(data_type):
            if int(data_type[1]) > 1 and not int(data_type[1]) % 8:
                return True
        return False

    @classmethod
    def separate_type_base_and_length(cls, data_type: str) -> Tuple[str, str]:
        for i in range(len(data_type)):
            if data_type[i].isnumeric():
                return data_type[0:i], data_type[i:]
        return data_type, ''


class StructDataMember(StructMember):
    """This class is used to represent a data member in a struct"""

    def __init__(self, name: str, data_type: str, base: str, length: int):
        self.instance_name = name
        self.full_data_type = data_type
        self._data_type_base = base
        self._data_length_bytes = length

    @classmethod
    def from_DataType(cls, name: str, data_type: _DataType) -> 'StructMember':
        return StructDataMember(name,
                                data_type.full_data_type,
                                data_type.base_type,
                                data_type.data_length_bytes)

    def get_copy(self, new_name: str = '') -> 'StructMember':
        return StructDataMember(new_name or self.instance_name,
                                self.full_data_type,
                                self._data_type_base,
                                self._data_length_bytes)

    def _load_from_file(self, file: io.BufferedReader):
        self.data = file.read(self._data_length_bytes)
        self.parse()

    def print(self, level: int = 0):
        if level:
            print('  ' * level
                  + self.full_data_type
                  , self.instance_name
                  + ':'
                  , self.value)
        else:
            print(self.value, end='')

    def parse(self):
        if self._data_type_base == 'char':
            self.value = self.data.decode('ascii')
        elif self._data_type_base == 'int':
            self.value = int.from_bytes(self.data, 'little', signed=True)
        elif self._data_type_base == 'uint':
            self.value = int.from_bytes(self.data, 'little', signed=False)
        elif self._data_type_base == 'hex':
            self.value = '0x' + self.data.hex()
        else:
            self.value = self.data.hex()


class StructArray(StructMember):
    def __init__(self,
                 name: str,
                 element_data_type: str,
                 element_template: StructMember,
                 array_descriptor: str,
                 array_type: str,
                 array_end: Union[str, int]):
        self.instance_name = name
        self.full_element_data_type = element_data_type
        self.element_template = element_template
        self.full_array_descriptor = array_descriptor
        self._array_type = array_type
        self._array_end = array_end

    @classmethod
    def from_DataType(cls, name: str, data_type: '_DataType'):
        return StructArray(name,
                           data_type.full_data_type,
                           data_type.element_data_class.from_DataType('template', data_type),
                           data_type.full_array_descriptor,
                           data_type.array_type,
                           data_type.array_end)

    def get_copy(self, new_name: str = '') -> 'StructMember':
        return StructArray(new_name or self.instance_name,
                           self.full_element_data_type,
                           self.element_template,
                           self.full_array_descriptor,
                           self._array_type,
                           self._array_end)

    def _load_from_file(self, file: io.BufferedReader):
        self.elements = []

        if self._array_type == 'fixed':
            for i in range(self._array_end):
                member = self._make_new_member(i)
                member._load_from_file(file)
                self.elements.append(member)
        else:
            terminator_length = len(self._array_end) // 2
            i = 0
            while file.peek(terminator_length).hex()[0:terminator_length * 2]\
                    != self._array_end:
                member = self._make_new_member(i)
                member._load_from_file(file)
                self.elements.append(member)
                i += 1
            file.seek(terminator_length, 1)

    def print(self, level: int = 0):
        if isinstance(self.element_template, StructDataMember):
            print('  ' * level
                  + self.full_element_data_type
                  + self.full_array_descriptor
                  , self.instance_name
                  + ': ['
                  , end='')
            for element in self.elements:
                element.print()
                print(', ', end='')
            print('\b\b]')
        else:
            print('  ' * level
                  + self.full_element_data_type
                  + self.full_array_descriptor
                  , self.instance_name
                  + ':')
            for element in self.elements:
                element.print(level + 1)

    def _make_new_member(self, index: int) -> StructMember:
        return self.element_template.get_copy(self.instance_name + '[' + str(index) + ']')


class _StructTemplate:
    """This class is used to cache structs from a struct file and is internal"""

    # it is initialized with just its name and location
    # the struct is loaded on demand so that unused structs aren't loaded
    def __init__(self, data_type: str, location: int):
        self.data_type = data_type
        # location of start of struct in struct file
        self.location = location
        # holds a name and data type for each field
        # the data type can represent a data field, array, or another template
        self.members: List[Tuple[str, _DataType]] = []
        # whether the template has been loaded from file yet
        self.is_loaded = False
        # keeps track of whether this template is in the process
        # of being loaded, protects against recursive structs
        self.is_being_loaded = False

    @classmethod
    def from_file(cls, s_file: str, root_struct_name: str):
        if not os.path.isfile(s_file):
            raise ValueError('File does not exist')
        struct_file = open(s_file, 'r')
        # read all of the structs to get their location
        # and make sure there are no duplicate structs
        root_template = None
        known_structs = []
        known_struct_names = []
        line = struct_file.readline()
        while line:
            # remove comments
            line = line.split('//')[0]
            # if the line starts with 'struct' and has an open bracket,
            # it is considered a valid struct declaration
            if line.find('{') >= 9 and line.startswith('struct '):
                struct_name = line[7:line.find('{') - 1]
                # check that there are no other structs with this name
                if '{'.join(known_struct_names).find('{' + struct_name + '{')\
                        == -1:
                    known_structs.append(_StructTemplate(struct_name,
                                                         struct_file.tell()))
                    known_struct_names.append(struct_name)
                    if struct_name == root_struct_name:
                        root_template = known_structs[-1]
                else:
                    raise ValueError('Multiple structs with name',
                                     struct_name, 'declared')
            line = struct_file.readline()
        # load the first struct template, which will in turn
        # load all the templates that are referenced
        if root_struct_name:
            if not root_template:
                raise ValueError('Desired root struct', root_struct_name,
                                 'does not exist')
        else:
            root_template = known_structs[0]
        root_template.load_template(struct_file, known_structs)
        struct_file.close()
        return root_template

    # read the file to load the layout of this struct
    def load_template(self, struct_file, known_structs):
        if self.is_loaded:
            return
        if self.is_being_loaded:
            raise ValueError('Struct', self.data_type, 'is recursive')
        self.is_being_loaded = True
        struct_file.seek(self.location)
        line = struct_file.readline()
        # read until we reach the end of the struct or file
        while line and not line[0] == '}':
            while line[0].isspace():
                line = line[1:]
            # read the data type of the next field
            d_type = ''
            while not line[0].isspace():
                d_type += line[0]
                line = line[1:]
            if not line:
                raise ValueError('No name specified for member with type', d_type)
            while line[0].isspace():
                line = line[1:]
            # read the field name
            field_name = ''
            while not line[0].isspace():
                field_name += line[0]
                line = line[1:]
            # parse the data type and load it if it is a template
            data_type = _DataType(d_type, known_structs)
            if (data_type.data_class == StructArray
                    and data_type.element_data_class == StructObject)\
                    or data_type.data_class == StructObject:
                previous_position = struct_file.tell()
                data_type.struct_template.load_template(struct_file, known_structs)
                struct_file.seek(previous_position)
            self.members.append((field_name, data_type))
            line = struct_file.readline()
        # indicate the struct is now loaded
        self.is_being_loaded = False
        self.is_loaded = True


class StructObject(StructMember):

    def __init__(self, name: str, data_type: str, members: Dict[str, StructMember]):
        self.instance_name = name
        self.data_type = data_type
        self.members = members

    @classmethod
    def from_DataType(cls, name: str, data_type: '_DataType') -> 'StructMember':
        return StructObject.from_template(name, data_type.struct_template)

    def get_copy(self, new_name: str = '') -> 'StructMember':
        member_names = list(self.members.keys())
        member_objects = []
        for member_object in self.members.values():
            member_objects.append(member_object.get_copy())
        members = dict(zip(member_names, member_objects))
        return StructObject(new_name or self.instance_name, self.data_type, members)

    def _load_from_file(self, file: io.BufferedReader):
        for member in self.members.values():
            member._load_from_file(file)

    def print(self, level: int = 0):
        print('  ' * level
              + self.data_type
              , self.instance_name)
        for member in self.members.values():
            member.print(level + 1)

    @classmethod
    def from_template(cls, name: str, template: _StructTemplate) -> 'StructObject':
        member_names: List[str] = []
        member_objects: List[StructMember] = []
        for member_tuple in template.members:
            m_name = member_tuple[0]
            d_type = member_tuple[1]
            m_object = d_type.data_class.from_DataType(m_name, d_type)
            member_names.append(m_name)
            member_objects.append(m_object)
        members = dict(zip(member_names, member_objects))
        return StructObject(name, template.data_type, members)

    @classmethod
    def from_file(cls, s_file: str, root_struct_name: str = ''):
        template = _StructTemplate.from_file(s_file, root_struct_name)
        return StructObject.from_template('file', template)

    def load_data_from_file(self, file: str):
        if not os.path.isfile(file):
            raise ValueError('File does not exist')
        d_file = open(file, 'rb')
        self._load_from_file(d_file)
        d_file.close()
