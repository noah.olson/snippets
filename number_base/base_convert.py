# base_convert.py
# Copyright 2020 Noah Olson
# This software is released under the MIT license (included at the end of this file)


import sys


NUMERAL_LUT = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
]

VALUE_LUT = {
    '0': 0,
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    'A': 10,
    'B': 11,
    'C': 12,
    'D': 13,
    'E': 14,
    'F': 15,
}


def base_n_to_10(number: str, base: int) -> int:
    number = number.upper()
    result = 0
    place = len(number) - 1
    place_value = 1
    while place >= 0:
        result += place_value * VALUE_LUT[number[place]]
        place_value *= base
        place -= 1
    return result


def base_10_to_n(number: int, base: int) -> str:
    place = 0
    while base ** (place + 1) <= number:
        place += 1
    result = ''
    remainder = number
    while place >= 0:
        value = remainder // (base ** place)
        result += NUMERAL_LUT[value]
        remainder -= value * (base ** place)
        place -= 1
    return result


def is_valid_base_n_number(number: str, base: int) -> bool:
    number = number.upper()
    max_value = base - 1
    for char in number:
        if char not in VALUE_LUT or VALUE_LUT[char] > max_value:
            return False
    return True


def can_convert(number: str, base_1: str, base_2: str) -> bool:
    if not (base_1.isnumeric() and base_2.isnumeric()):
        print('Error: base is not a number')
        return False
    base_1 = int(base_1)
    base_2 = int(base_2)
    if 2 > base_1 or base_1 > 16 or 2 > base_2 or base_2 > 16:
        print('Error: unsupported base')
        return False
    if not is_valid_base_n_number(number, base_1):
        print('Error:', number, 'is not a positive base', base_1, 'number')
        return False
    return True


def convert(number: str, base_1: str, base_2: str) -> str:
    return base_10_to_n(base_n_to_10(number, int(base_1)), int(base_2))


def print_help():
    print('\nThis program converts positive numbers between bases.\n'
          'It can handle bases from 2 to 16.\n'
          'It requires 3 arguments:\n'
          '1: number: number to convert\n'
          '2: base 1: base of the number\n'
          '3: base 2: base you want to convert the number to\n')


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print_help()
        exit()
    if can_convert(sys.argv[1], sys.argv[2], sys.argv[3]):
        print(convert(sys.argv[1], sys.argv[2], sys.argv[3]))


"""
MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
