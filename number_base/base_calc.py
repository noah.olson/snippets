# base_calc.py
# depends on base_convert.py
# Copyright 2020 Noah Olson
# This software is released under the MIT license (included at the end of base_convert.py)


import sys
import base_convert


OPERATORS = {
    '+': lambda num_1, num_2: num_1 + num_2,
    '-': lambda num_1, num_2: num_1 - num_2,
    '/': lambda num_1, num_2: num_1 // num_2,
    '*': lambda num_1, num_2: num_1 * num_2,
    '%': lambda num_1, num_2: num_1 % num_2,
    'pow': lambda num_1, num_2: num_1 ** num_2,
    'or': lambda num_1, num_2: num_1 | num_2,
    'xor': lambda num_1, num_2: num_1 ^ num_2,
    'and': lambda num_1, num_2: num_1 & num_2,
}


def can_calculate(num_1: str, base_1: str, operator: str, num_2: str, base_2: str) -> bool:
    if not base_convert.can_convert(num_1, base_1, '10'):
        return False
    if not base_convert.can_convert(num_2, base_2, '10'):
        return False
    if operator not in OPERATORS:
        print('Error: unrecognized operator')
        return False
    return True


def calculate(num_1: str, base_1: str, operator: str, num_2: str, base_2: str) -> int:
    num_1 = base_convert.base_n_to_10(num_1, int(base_1))
    num_2 = base_convert.base_n_to_10(num_2, int(base_2))
    return OPERATORS[operator](num_1, num_2)


def print_help():
    print('\nThis program calculates a result from two positive base n numbers.\n'
          'It can handle bases from 2 to 16.\n'
          'It requires 5 or 6 arguments:\n'
          '1: 1st number\n'
          '2: base of 1st number\n'
          '3: operator - one of: + - / * % pow or xor and\n'
          '4: 2nd number\n'
          '5: base of 2nd number\n'
          '6: (optional) base of the result\n')


if __name__ == '__main__':
    if len(sys.argv) != 6 and len(sys.argv) != 7:
        print_help()
        exit()
    if not can_calculate(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]):
        exit()

    result = calculate(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    if len(sys.argv) == 6:
        print(result)
    elif len(sys.argv) == 7:
        if not base_convert.can_convert('0', '10', sys.argv[6]):
            print('Error: result base is not supported')
            exit()
        if result < 0:
            print('Warning: result is negative and could not be converted; outputting in base 10')
            print(result)
            exit()
        print(base_convert.base_10_to_n(result, int(sys.argv[6])))
