# truth_table_gen version 0.1
# Copyright (c) 2020 Noah Olson. See LICENSE at end of file.

import enum
from typing import List, Dict
import os

try:
    from colorama import Fore, Style
    import os
    if os.name == 'nt':
        os.system('color')
    color = True
except:
    color = False
if color:
    def print_color(text: str):
        if text.replace(' ', '') == 'T':
            print(Fore.GREEN + text + Fore.RESET, end='')
        elif text.replace(' ', '') == 'F':
            print(Fore.RED + text + Fore.RESET, end='')
        else:
            print(Fore.BLUE + text + Fore.RESET, end='')
else:
    def print_color(text: str):
        print(text, end='')



class Node_Type(enum.Enum):
    BRANCH = enum.auto()
    LEAF = enum.auto()


class Node:
    def __init__(self, type_: Node_Type):
        self.type_ = type_
        if type_ == Node_Type.BRANCH:
            # left leaf
            self.left: Node = None
            # right leaf (stays None when the operation is negation)
            self.right: Node = None
            # operator that joins the leaves
            self.join: str = None
        else:
            # a basic statement
            self.term: str = None

    def to_string(self, inner: bool = False) -> str:
        if self.type_ == Node_Type.BRANCH:
            string = ''
            if inner:
                string += '('
            if self.right:
                string += self.left.to_string(True)
                string += self.join
                string += self.right.to_string(True)
            else:
                string += self.join
                string += self.left.to_string(True)
            if inner:
                string += ')'
            return string
        else:
            return self.term

    def solve(self, truth_dict: Dict[str, str]) -> str:
        if self.type_ == Node_Type.BRANCH:
            if self.join == '~':
                left = self.left.solve(truth_dict)
                return 'T' if (left == 'F') else 'F'

            left = self.left.solve(truth_dict)
            right = self.right.solve(truth_dict)
            if self.join == '^':
                return 'T' if (left == 'T' and right == 'T') else 'F'
            elif self.join == 'V':
                return 'T' if (left == 'T' or right == 'T') else 'F'
            elif self.join == '->':
                return 'F' if (left == 'T' and right == 'F') else 'T'
            elif self.join == '<->':
                return 'T' if (left == right) else 'F'
        else:
            return truth_dict[self.term]


# returns None on error
def build_binary_tree(string: str) -> Node:
    # turn parenthesized parts into single tokens
    nest_depth = 0
    tokens = []
    current_token = ''
    i = -1
    for char in string:
        i += 1
        if char == '(':
            nest_depth += 1
            if nest_depth == 1:
                if current_token:
                    tokens.append(current_token)
                current_token = char
            else:
                current_token += char
        elif char == ')':
            nest_depth -= 1
            if nest_depth == 0:
                current_token += char
                tokens.append(current_token)
                current_token = ''
            elif nest_depth < 0:
                if current_token:
                    tokens.append(current_token)
                print('Error: closing parenthese without opening parenthese after:')
                print(tokens)
                return None
            else:
                current_token += char
        else:
            current_token += char

    if current_token:
        tokens.append(current_token)

    if nest_depth:
        print('Error: missing closing parenthese')
        return None


    # separate tokens containing a separator
    SEPARATOR_CHARACTERS = '^V<->'
    old_tokens = tokens
    tokens = []
    for token in old_tokens:
        if token[0] == '(':
            tokens.append(token)
            continue

        current_token = ''
        parsing_separator = False
        for char in token:
            if parsing_separator:
                if char in SEPARATOR_CHARACTERS:
                    current_token += char
                else:
                    tokens.append(current_token)
                    current_token = char
                    parsing_separator = False
            else:
                if char in SEPARATOR_CHARACTERS:
                    if current_token:
                        tokens.append(current_token)
                    current_token = char
                    parsing_separator = True
                else:
                    current_token += char

        if current_token:
            tokens.append(current_token)

    # negations that apply to parenthesized terms will be at the end of
    # the regular term preceding them
    # so, move the negation operator to parenthesized terms
    i = 0
    while i < len(tokens) - 1:
        while tokens[i] and tokens[i][-1] == '~':
            if tokens[i + 1][0] not in SEPARATOR_CHARACTERS:
                tokens[i + 1] = '~' + tokens[i + 1]
                tokens[i] = tokens[i][:-1]
            else:
                print('Error: cannot use negation operator in front of another operator')
                print('~ in front of ' + tokens[i + 1])
                return None

        if not tokens[i]:
            tokens.pop(i)
        else:
            i += 1

    # tokens is now grouped properly into terms, parenthesized terms, and operators

    # check pattern: number of tokens must be odd, terms must be separated by operators
    if not (len(tokens) % 2):
        print('Error: even number of tokens, must be missing an operator or term')
        return None
    SEPARATORS = ['^', 'V', '->', '<->']
    i = 1
    for token in tokens:
        if i % 2:
            if token[0] in SEPARATOR_CHARACTERS:
                print('Error: operator where there should be a term')
                if i >= 2:
                    print(token + ' between ' + tokens[i - 2] + ' and ' + tokens[i])
                else:
                    print(token + ' in front of ' + tokens[i])

                return None
        else:
            if token not in SEPARATORS:
                print('Error: term where there should be an operator (are there 2 operators in a row?)')
                print(token + ' between ' + tokens[i - 2] + ' and ' + tokens[i])
                return None
        i += 1

    # at this point, tokens should only contain valid input

    # build a binary tree

    # returns None on error
    def group_by_precedence(tokens: List[str]) -> Node:
        # operator precedence:
        # 1. <-> (biconditional)
        # 2. -> (conditional)
        # 3. ^ and V (conjunction and disjunction)
        if len(tokens) == 1:
            if tokens[0][0] == '(':
                node = build_binary_tree(tokens[0][1:-1])
            elif tokens[0][0] == '~':
                node = Node(Node_Type.BRANCH)
                node.left = group_by_precedence([tokens[0][1:]])
                node.join = '~'
            else:
                node = Node(Node_Type.LEAF)
                node.term = tokens[0]
            return node

        # separators grouped and ordered by precedence
        SEPARATORS = [['<->'], ['->'], ['^', 'V']]
        for operator_group in SEPARATORS:
            first_index = -1
            first_operator = None
            for operator in operator_group:
                if operator in tokens:
                    if not first_operator or tokens.index(operator) < first_index:
                        first_operator = operator
                        first_index = tokens.index(operator)
            if first_operator:
                # we actually want to allow multiple ^ and V operators at the same
                # level if they are the same operator because of the commutative property
                check_operator = first_operator if len(operator_group) == 1 else \
                    (operator_group[1] if first_operator == operator_group[0] else operator_group[0])
                if check_operator in tokens[first_index + 1:]:
                    print('Error: multiple operators of the same precedence at the same level')
                    print(first_operator + ' and ' + check_operator)
                    return None

                left = group_by_precedence(tokens[:first_index])
                right = group_by_precedence(tokens[first_index + 1:])
                if not (left or right):
                    return None
                node = Node(Node_Type.BRANCH)
                node.left = left
                node.right = right
                node.join = first_operator
                return node

    return group_by_precedence(tokens)


# returns None on error
def build_truth_table(input_: str) -> List[List[str]]:
    root = build_binary_tree(input_)
    if not root:
        return None

    # make a list of basic terms
    def get_terms_in_node(term_list: List[str], node: Node):
        if node.type_ == Node_Type.BRANCH:
            get_terms_in_node(term_list, node.left)
            # branch nodes with ~ (negation operator) will not have a right branch
            if node.right:
                get_terms_in_node(term_list, node.right)
        else:
            if node.term not in term_list:
                term_list.append(node.term)

    terms: List[str] = []
    get_terms_in_node(terms, root)
    terms.sort()

    # generate truth values for terms
    inputs: List[List[str]] = []
    for i in range(2 ** len(terms)):
        row = []
        for j in range(len(terms)):
            column_val = not bool((i // (2 ** j)) % 2)
            row.append('T' if column_val else 'F')
        row.reverse()
        inputs.append(row)

    # make a list of the columns to show
    def get_column_list(column_list: List[Node], node: Node):
        if node.type_ == Node_Type.BRANCH:
            get_column_list(column_list, node.left)
            if node.right:
                get_column_list(column_list, node.right)
            column_list.append(node)

    columns: List[Node] = []
    get_column_list(columns, root)

    # get the column titles
    column_titles: List[str] = []
    for column in columns:
        column_titles.append(column.to_string())

    # solve
    outputs: List[List[str]] = []
    for input_group in inputs:
        input_dict = dict(zip(terms, input_group))
        output_group = []
        for column in columns:
            output_group.append(column.solve(input_dict))
        outputs.append(output_group)

    # build a table from terms, column_titles, inputs, and outputs
    table: List[List[str]] = []
    title_row = []
    for term in terms:
        title_row.append(term)
    # leaves an empty column in csv for readability
    title_row.append('')
    for column_title in column_titles:
        title_row.append(column_title)
    table.append(title_row)
    for i in range(len(inputs)):
        row = []
        for inp in inputs[i]:
            row.append(inp)
        # leaves an empty column in csv for readability
        row.append('')
        for output in outputs[i]:
            row.append(output)
        table.append(row)

    return table


def tt_command(input_: str):
    if input_ == 'help':
        print('generate a truth table for a statement\n'
              '() not []'
              '~ is negation\n'
              '^ is conjunction\n'
              'V is disjunction\n'
              '-> is conditional\n'
              '<-> is biconditional\n')
        return

    input_ = input_.replace(' ', '')
    input_ = input_.replace('\t', '')
    if not input_:
        print('Error: no statement to parse')
        return

    table = build_truth_table(input_)
    if not table:
        return

    max_column_width = []
    for column in range(len(table[0])):
        max_column_width.append(0)
        for row in range(len(table)):
            if len(table[row][column]) > max_column_width[column]:
                max_column_width[column] = len(table[row][column])

    for row in range(len(table)):
        for column in range(len(table[0])):
            print_color(table[row][column] + ' '*(max_column_width[column] - len(table[row][column]) + 2))
        print('')


def ttcsv_command(input_: str):
    if input_ == 'help':
        print('same thing as tt, except outputs to tt.csv instead of console\n'
              'running this multiple times will append to tt.csv if it already exists\n')
        return

    input_ = input_.replace(' ', '')
    input_ = input_.replace('\t', '')
    if not input_:
        print('Error: no statement to parse')
        return

    table = build_truth_table(input_)
    if not table:
        return

    try:
        out_file = open('tt.csv', 'a')
        for row in table:
            for column in row:
                out_file.write(column + ',')
            out_file.write('\n')
        out_file.write('\n')

        out_file.close()

        print('Table written to ' + os.path.abspath('tt.csv'))
    except:
        print('Error: could not write to file')


def main():
    COMMANDS = {'tt': tt_command,
                'ttcsv': ttcsv_command,
                }

    while True:
        line = input('\nEnter command: ')
        if line.lower() in ['quit', 'exit', 'q']:
            exit()

        command = ''
        command_input = ''
        if ' ' in line:
            command = line[:line.find(' ')]
            command_input = line[line.find(' ') + 1:]
        else:
            print('Error: no argument for command')
            continue

        if command not in COMMANDS:
            print('Error: unrecognized command: ' + command)
            continue

        COMMANDS[command](command_input)


if __name__ == '__main__':
    main()


"""
MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
